//[Activity]

//[MAIN GOAL] Create an Add Course Page

// 1. Acquire all needed components to build the Page
// 2. Identify all the info needed to create a new course. (name, desc, price)
// 3. Expose the data to the entry point and assign a designated enpoint for the page. 
// 4. Re-render the navbar to add a new nav item component for the Add Course Page.
// 5. Create a simulation that will describe the workflow if the user would want to create a new course. 
// 6. Submit the project repo in boodle
//      WDC028-47 | React.js - Effects, Events and Forms
//     Demo Video of the simulation in GC
// ========================================================================
//   30 MINS. Resume (3:45 PM)