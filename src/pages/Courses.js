//Identify which components will be displayed
import { useState, useEffect } from 'react'; 
import Hero from './../components/Banner'; 
import CourseCard from './../components/CourseCard';
import { Container } from 'react-bootstrap'


const bannerDetails = {
   title: 'Course Catalog',
   content: 'Browse through our Catalog of Courses'
}

//Catalog all Active courses
   //1. Need Container Component for each course that we will retrieve from the database
   //2. Declare state for the courses collection, by default it is an empty array.
   //3. Create a side effect that will send a request to our backend api project. 
   //4. Pass down the retrieved resources inside the component as props.

export default function Courses() {
    
    //this array/storage will be used to save our course components for all active courses.
	const [coursesCollection, setCourseCollection] = useState([]);

	//create an 'effect' using our Effect Hook, provide a list of dependencies. 
	//the effect that we are going to create is fetching data from the server.
	useEffect(() => {
		//fetch() => is a JS method which allows us to pass/create a request to an API.
		//SYNTAX: fetch(<request URL>, {OPTIONS})
		//GET HTTP METHOD especially since we do not need to insert a req body or pass a token => NO NEED TO INSERT OPTIONS. 

		//remember that once you send a request to an endpoint, a 'promise' is returned as a result. 
		//A promise has 3 states: (fulfilled, reject, pending)
		//we need to be able to handle the outcome of the promise. 
		//we need to make the response usable on the frontend side. we need to convert it to a json format.
		//upon converting the fetched data to a json format a new promise will be executed
		fetch('https://whispering-spire-20350.herokuapp.com/courses/').then(res => res.json()).then(convertedData => {
			//console.log(convertedData);
			//we want the resources to be displayed in the page
			//since the data is stored in an array storage, we will iterate the array to explicitly get each document one by one.
			//there will be multiple course card component that will rendered that corresponds to each document retrieved from the database. we need to itilize a storage to contain the cards that will be rendered.
			//key attribute => as a reference to avoid rendering duplicates of the same element/object.
			//arrange the cards by 3s and make sure the card is responsive to device changes.
			setCourseCollection(convertedData.map(course => {
				return(
					<CourseCard key={course._id} courseProp={course} />
				)
			})) 
		});
	},[]); 
    
    //send a demo video in HO of your milestone for the Reactive Register Page.
    //link the project repo here:
    //WDC028-49 | React.js - State Management
    //WDC028-50 | React.js - API Integration with Fetch (8:15 am tomorrow)

	return(
	  <>
		<Hero bannerData={bannerDetails} />
		<Container> 
			{coursesCollection}
		</Container>
	  </>
	);
};