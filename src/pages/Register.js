//identify the components needed to create the register page.
import { useState, useEffect, useContext } from 'react'; 
import Hero from '../components/Banner';
import { Container, Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2'; 
import UserContext from '../UserContext'; 

const data = {
  title: 'Welcome to the Register Page',
  content: 'Create an Account to Enroll'
}

//declare a state for the input fields in the register page.
//create a 'side effect' on the page to render the button component if the conditions are not yet met.

export default function Register() {
    
    //destructure the data from the context object for the user.
	const { user } = useContext(UserContext); //{}
    
    //we will declare a state for the following components to make sure that they will be empty from the start.
    //once you passed down the variables into their respective components they will bounded to its value.
    //for us to manage the 'states' of our component we will need a 'Hook' in React that will allow to handle state changes within the component
    //using the acquired Hook from react, we will now apply a 2 way binding for the input elements.
    //syntax: [getter, setter] = useState(value) 
    //naming convention for state setters:
    // set + Variable
    //Contain variables inside a 1 dimensional structure
    //we will use an eventEmitter that will allow us to change the states of the component.
    //why Object Object? => Type Coersion
    //[] + [] = 
    // {} + [] = type coercion
    //ATM we applied/created a 2 way binding in our form component, this 2 way binding will allow us to create and manage information seamlessly across all our components. 
    //check if you successfully caught the data in each component.
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');//password confirmation

	//declare a state for the button component, attack a state 'hook' to eventually manage and change the state of the button
	//to eventually change the state of the button, insert a state setter
	//now using the state hook, apply a conditional rendering to our Button component.
	//naming convention for boolean values?
	  // is (prefix) + Verb
	  // => YES OR NO / TRUE OR FALSE
	const [isActive, setIsActive] = useState(false);
	//make a reactive response section for the password using conditional rendering.  
	//we need a way to eventually modify and change the state of this component (setter).
	const [isMatched, setIsMatched] = useState(false); 
	const [isMobileValid, setIsMobileValid] = useState(false);
	//state for the heading section of the form.
	const [isAllowed, setIsAllowed] = useState(false); 
  	
  	//describe the side effects/effects that will react on how the user will interact with form.
  	//to remove the warnings we will provide the dependencies list.
    useEffect(() => {
		//verify if you are able to catch all values from the control components

		//validate the data from the registration form to make sure that all inputs are clean before trying to perform a register function. 
		//NEW TASK: MAKE THE REACTIONS SWIFT.

		//we will modify the control structure to best suit our set rules bound by the register page. (LONG VERSION)
		if (
			//check if mobile is valid
			mobileNo.length === 11
		)
			{
				//this block of code will run if the 1st set of rule above was met. 
				setIsMobileValid(true)	
				if (
					//password checker if match
					password1 === password2 && password1 !== '' && password2 !== '' 
				) {
					setIsMatched(true);
				    if (firstName !== '' && lastName!== ''  && email!== '' ) {
						setIsAllowed(true); 
						setIsActive(true);
				    } else {
				    	setIsAllowed(false); 
						setIsActive(false);
				    }
				} else {
					setIsMatched(false);
					setIsAllowed(false);
					setIsActive(false);
				}
			} 
		else if(password1 !== '' && password1 === password2) {
			setIsMatched(true);
		}
		else {
			//Not permitted
			setIsActive(false);
			setIsMatched(false);
			setIsMobileValid(false);
			setIsAllowed(false);
		}; 
    },[firstName, lastName, email, password1,password2, mobileNo]); 
    //the Effect hook 'useEffect()' will use the dependencies list in order to identify which components to watch out for changes. to avoid continous form submission for every render of each component. 

    //catch the 'click' event that will happen on the button component
    //we can make it as async, to make sure that the response will be rendered out first
	const registerUser = async (eventSubmit) => {
		eventSubmit.preventDefault()
		// console.log(firstName);
		// console.log(lastName);
		// console.log(email);
		// console.log(mobileNo);
		// console.log(password1);
		// console.log(password2);

		//we will now modify this function so that we will be able to send a request to our API so that the client will be able to create his own account. 
		//syntax: fetch('url', {options})

		//keep in mind upon sending the data to the api it should be all text. 
		//upon sending this request to create an account a promise will be initialized

		//include an await expression in the fetch to make sure that react will finish the task first before proceeding to the next
		const isRegistered = await fetch('https://whispering-spire-20350.herokuapp.com/users/register', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				password: password1, 
				mobileNo: mobileNo
			})
		}).then(response => response.json())
		.then(dataNakaJSON => {
			//console.log(dataNakaJSON); //user object
			//create a control structure if success or fail. 
			//check if the email props has data.
			if (dataNakaJSON.email) {
				return true; //SUCCESS
			} else {
				//failure 
				return false; 
			}
		})

		//Create a control structure that will evaluate the result of the fetch method.
		if (isRegistered) {
           //prompt message, clear the components, redirect the user to the login if successful registration. 
			//display a message that will confirm to the user that registration is successful.
			await Swal.fire({
				icon: 'success',
				title: 'Registration Successful',
				text: 'Thank you for creating an Account'
			});
			//reset the registration form
	        setFirstName('');
	        setLastName('');
	        setEmail('');
	        setMobileNo('');
	        setPassword1('');
	        setPassword2('');
			//redirect the user to the login page for authentication.
			window.location.href = "/login"; 	
		} else {
 			//response if registration has failed
 			Swal.fire({
 				icon: 'error',
 				title: 'Something Went Wrong',
 				text: 'Try Again Later!' 
 			});
		}
	};


	return(
		user.id
		?
		   <Navigate to="/courses" replace={true} />
		:
	    <>
			<Hero bannerData={data}/>
			<Container>
			    {/*Form Heading*/}
			    {
			    	isAllowed ? 
						<h1 className="text-center text-success">You May Now Register!</h1>
			    	:
						<h1 className="text-center">Register Form</h1>
			    }
				<h6 className="text-center mt-3 text-secondary">Fill Up the Form Below</h6>
				
				{/*Form*/}
				<Form onSubmit={e => registerUser(e)}>
				   {/*First Name Field*/}
				   <Form.Group>
				   		<Form.Label>First Name: </Form.Label>
				   		<Form.Control type="text" 
				   		   placeholder="Enter your first Name"
				   		   required 
				   		   value={firstName}
				   		   onChange={event => setFirstName(event.target.value)}
				   		/>
				   </Form.Group>

				   {/*Last Name Field*/}
				   <Form.Group>
				   		<Form.Label>Last Name:</Form.Label>
				   		<Form.Control 
				   			type="text"
				   			placeholder="Enter your Last Name"
				   			required
				   			value={lastName}
				   			onChange={e => setLastName(e.target.value)}
				   		/>
				   </Form.Group>

	               {/*Email Address Field*/}
				   <Form.Group>
				   		<Form.Label>Email:</Form.Label>
				   		<Form.Control
				   		   type="email" 
				   		   placeholder="Insert your Email Address"
				   		   required
				   		   value={email}
				   		   onChange={e => setEmail(e.target.value)}
				   		/>
				   </Form.Group>

				   {/*Mobile Number Field*/}
				   {/*Customize this component so that you will get the correct format for the mobile Number*/}
				   <Form.Group>
				   		<Form.Label>Mobile Number:</Form.Label>
				   		<Form.Control 
				   			type="number"
				   			placeholder="Insert your Mobile No."
				   			required
				   			value={mobileNo}
				   			onChange={e => setMobileNo(e.target.value)}
				   		/>
				   		{
				   			isMobileValid ?
				   				<span className="text-success">Mobile No. is Valid!</span>
				   			:
				   				<span className="text-muted">Mobile No. Should Be 11-digits</span>
				   		}
				   </Form.Group>

				   {/*Password Field*/}
				   <Form.Group>
				   	  <Form.Label>Password:</Form.Label>
				   	  <Form.Control 
				   	  	 type="password"
				   	  	 placeholder="Enter your password"
				   	  	 required
				   	  	 value={password1}
				   	  	 onChange={e => setPassword1(e.target.value)}
				   	  />
				   </Form.Group>

				   {/*Confirm Password Field*/}
				   <Form.Group>
				   	  <Form.Label>Confirm Password:</Form.Label>
				   	  <Form.Control 
				   	  	 type="password"
				   	  	 placeholder="Confirm your password"
				   	  	 required
				   	  	 value={password2}
				   	  	 onChange={e => setPassword2(e.target.value)}
				   	  />
				   	  {
				   	  	isMatched ? 
				   	  		<span className="text-success">Passwords Matched!</span>
				   	  	:
						   	<span className="text-danger">Passwords Should Match!
						   	</span> 	  	
				   	  }
				   </Form.Group>

				   {/*Register Button*/}
				   {
				   	 isActive ? 
					    <Button 
					      className="btn-success btn-block"
					      type="submit"
					    > 
					      Register 
					    </Button>
				   	 : 
					    <Button 
					      className="btn-secondary btn-block"
					      disabled
					    > 
					      Register 
					    </Button>
				   }

				</Form>
			</Container>
		</>
	);
};