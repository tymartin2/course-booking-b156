//identify which components are needed to build the navigation
import { useContext } from 'react'; 
import { Navbar, Nav, Container } from 'react-bootstrap'

//implement Links in the navbar
import { Link } from 'react-router-dom';

//Consumer? 
import UserContext from '../UserContext';

//we will now describe how we want our Navbar to look.
function AppNavBar() {
  
  //lets destructure the context object and 'consume' the needed information to properly render the navbar. 
  const { user } = useContext(UserContext); 
   
	return(
      <Navbar bg="primary" expand="lg">
        <Container>
        	<Navbar.Brand> B156 Booking App </Navbar.Brand>  
        	<Navbar.Toggle aria-controls="basic-navbar-nav" />
        	<Navbar.Collapse> 
        	   <Nav className="ml-auto">
                  <Link to="/" className="nav-link">
                      Home 
                  </Link>
                  {user.id !== null ? 
                     <Link to="/logout" className="nav-link">
                        Logout
                     </Link>
                    :
                    <>
                     <Link to="/register" className="nav-link">
                        Register 
                    
                    </Link>
                     <Link to="/login" className="nav-link">
                        Login
                    </Link>
                    </>
                  }
                  

                  <Link to="/courses" className="nav-link">
                      Courses
                  </Link>
        	   </Nav>
        	</Navbar.Collapse>  
        </Container>
      </Navbar>
	);
};

export default AppNavBar; 